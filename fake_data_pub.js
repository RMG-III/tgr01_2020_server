var mqtt   = require('mqtt');
var client = mqtt.connect('mqtt://202.139.192.75/tgr2020/#');
 
client.on('connect', function () {
    console.log("MQTT Connected.");

    setInterval(function() {
        console.log(new Date().toISOString())
        let data = {
            "DevEUI_uplink":{
                "Time": new Date().toISOString() ,
                "DevEUI":"BE7A000000000409",
                "DevAddr":"BE7A0009",
                "FPort":"2","FCntUp":"90",
                "ADRbit":"1","MType":"4",
                "FCntDn":"90",
                "payload_hex":"00000000000000fe000000",
                "mic_hex":"1f96ff96",
                "Lrcid":"00000231",
                "LrrRSSI":"-111.000000",
                "LrrSNR":"4.750000",
                "SpFact":"7",
                "SubBand":"G1",
                "Channel":"LC16",
                "DevLrrCnt":"1",
                "Lrrid":"1000018F",
                "Late":"0",
                "LrrLAT":"19.027521",
                "LrrLON":"99.891396",
                "Lrrs": {
                    "Lrr":[{"Lrrid":"1000018F","Chain":"0","LrrRSSI":"-111.000000","LrrSNR":"4.750000","LrrESP":"-112.254700"}]}
                    ,"CustomerID":"1100001293"
                    ,"CustomerData":{"alr":{"pro":"LORA/Generic","ver":"1"}}
                    ,"ModelCfg":"0"
                    ,"InstantPER":"0.000000"
                    ,"MeanPER":"0.000000"}
                }

        let track_data_01 =
        {
            "mac_addr" : "80:E1:26:07:E1:BF",
            "rssi" : -66,
            "timestamp" : "2020-01-10 11:39:55.050092",
            "event_code" : 1
        }
        let track_data_16 =
        {
            "mac_addr" : "80:E1:26:00:B8:89",
            "rssi" : -59,
            "timestamp" : "2020-01-10 11:39:57.050106",
            "event_code" : 1
        }
        let track_data_34 =
        {
            "mac_addr" : "80:E1:26:07:C7:BE",
            "rssi" : -78,
            "timestamp" : "2020-01-10 11:39:59.050110",
            "event_code" : 1
        }
        //client.publish('tgr2020/pm25/data/01', JSON.stringify(data));
        client.publish('tgr2020/track/data/1', JSON.stringify(track_data_01));
        client.publish('tgr2020/track/data/1', JSON.stringify(track_data_16));
        client.publish('tgr2020/track/data/1', JSON.stringify(track_data_34));
        console.log("publish new data")
    }, 5000)
})

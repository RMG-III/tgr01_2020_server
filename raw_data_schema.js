const testSchema = new Schema({
  DevEUI_uplink: {
    type: Object,
    properties: {
      Time: {
        type: String
      },
      DevEUI: {
        type: String
      },
      DevAddr: {
        type: String
      },
      FPort: {
        type: String
      },
      FCntUp: {
        type: String
      },
      ADRbit: {
        type: String
      },
      MType: {
        type: String
      },
      FCntDn: {
        type: String
      },
      payload_hex: {
        type: String
      },
      mic_hex: {
        type: String
      },
      Lrcid: {
        type: String
      },
      LrrRSSI: {
        type: String
      },
      LrrSNR: {
        type: String
      },
      SpFact: {
        type: String
      },
      SubBand: {
        type: String
      },
      Channel: {
        type: String
      },
      DevLrrCnt: {
        type: String
      },
      Lrrid: {
        type: String
      },
      Late: {
        type: String
      },
      LrrLAT: {
        type: String
      },
      LrrLON: {
        type: String
      },
      Lrrs: {
        type: Object,
        properties: {
          Lrr: {
            type: array,
            items: {
              type: Object,
              properties: {
                Lrrid: {
                  type: String
                },
                Chain: {
                  type: String
                },
                LrrRSSI: {
                  type: String
                },
                LrrSNR: {
                  type: String
                },
                LrrESP: {
                  type: String
                }
              }
            }
          }
        }
      },
      CustomerID: {
        type: String
      },
      CustomerData: {
        type: Object,
        properties: {
          alr: {
            type: Object,
            properties: {
              pro: {
                type: String
              },
              ver: {
                type: String
              }
            }
          }
        }
      },
      ModelCfg: {
        type: String
      },
      InstantPER: {
        type: String
      },
      MeanPER: {
        type: String
      }
    }
  }
},{versionKey:false,timestamps:false})

const express = require('express');
var path = require('path');
var io = require('@pm2/io')
var fs = require('fs');
// const DBCONNECT = require('./DBConnect')

const DBCONNECT = require('mongodb').MongoClient;
const URL = 'mongodb://tgr01_db:tgr01db@202.139.192.114:27017/TGR01_DB_Day01'

const OPTIONS = {
  useNewUrlParser: true,
  useUnifiedTopology: true
};


var mqtt   = require('mqtt');
var client = mqtt.connect('mqtt://202.139.192.75/tgr2020/#' , clientId = "TGR01");

const Schema = DBCONNECT.Schema

const app = express();
const port = process.env.PORT || 80;

app.use(express.static(path.join(__dirname,'./')));

let dataSizePerMin = 0;

//MQTT in Server

// const rawDataCollection = DBCONNECT.model('mqtt_raw_datas',testSchema)

client.on('connect', function () {
    console.log("MQTT Connected.");
    // client.subscribe('tgr2020/pm25/data/#');
    // client.subscribe('tgr2020/track/data/#');
    client.subscribe('sensor/#');

    setInterval(() => {
      latency.set(dataSizePerMin);
      dataSizePerMin = 0;
    },1000);
})
client.on('message', function (topic, message) {
     //console.log(topic.toString() + " => " + message.toString())
     var dataJSON = JSON.parse(message);

     console.log(JSON.stringify(dataJSON).length)
     dataSizePerMin += JSON.stringify(dataJSON).length;
     //console.log(dataSizePerMin);


     DBCONNECT.connect(URL ,OPTIONS,(err , result) => {
      if(err)
        throw err
      else {
        console.log("MongoDB connection Success")
      }
      var db = result.db("TGR01_DB_Day01");
      db.collection("mqtt_raw_datas").insertOne(dataJSON, function(err, res) {
        if (err) throw err;
        console.log("1 document inserted");
        result.close();
      });

    });
});

const latency = io.metric({
  name: 'MQTT latency',
  type: 'histogram',
  measurement: 'mean'
});

app.get('/' , (req , res) => {
  res.redirect(`/map/`);

  
});

app.get('/map/' , (req , res) => {
  res.sendFile(__dirname + '/Hello.html');
});

app.get('/map/mapData.json' , (req , res) => {


  DBCONNECT.connect(URL ,OPTIONS,(err , db) => {
    if(err)
      throw err
    else {
      console.log("MongoDB connection Success")
      var obj = {}
      var teamCheck = []
      var dbase = db.db("TGR01_DB_Day01");
      var query= dbase.collection("pm25_fill_data").find().sort({ts: -1}).toArray((err,result)=>{
        for(var x in result){
          //console.log(result[x]);
          if(teamCheck.indexOf(result[x]["team"]) == -1)
          {
            obj[x] = result[x];
            teamCheck.push(result[x]["team"])
          }
          if(teamCheck.length == 4)
          {
            break;
          }
        }
        console.log(teamCheck);
        var json = JSON.stringify(obj);
        fs.writeFile('mapData.json', json, 'utf8', (err,result)=>{
           if(err) throw err;
           else{
            console.log("Json File Created!");
            res.sendFile(__dirname + '/mapData.json');
           }
        });

      });
    }
  });

});
//Run Server on PORT 80
app.listen(port , () => {
  console.log(`App is running on port ${port}`);
});

const fs = require('fs')
const ini = require('ini')
const mqtt = require('mqtt')
const mongodb = require('mongodb')
var ISODate = require("isodate");

var config = ini.parse(fs.readFileSync(__dirname + '/config.ini', 'utf-8'))
var mqtt_client = mqtt.connect("mqtt://" + config.mqtt.host, {username: config.mqtt.username, password: config.mqtt.password, clientId: 'tgr01', connectTimeout: 3000, reconnectPeriod: 3000})
var mongo_client = mongodb.MongoClient
var db

mongo_client.connect(config.mongodb.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}, function(error, connection) {
    if (error) {
        console.error(error)
    } else {
        db = connection.db("TGR01_DB_Day01");
    }
})

function write_sensor_data(data , collection ) {
    if(collection == "pm25")
    {
        db.collection("pm25_raw_data").insertOne(data, function(error, res) {
            if(error) {
                console.error(error)    
            } else {
                //console.log("Insert data success");
            }
        })
    }

    if(collection == "track")
    {
        db.collection("track_raw_data").insertOne(data, function(error, res) {
            if(error) {
                console.error(error)    
            } else {
                //console.log("Insert data success");
            }
        })
    }
}
async function make_data_fillter(data)
{
    //console.log(data)
    var tmp = []
    for (var item in data) {

        fill_data = {}

        //fill_data['raw_data_id'] = data[item]['data']['_id']
        fill_data['device_id'] = data[item]['data']['DevEUI_uplink']['DevEUI']
        fill_data['device_addr'] = data[item]['data']['DevEUI_uplink']['DevAddr']
        fill_data['payload_hex'] = data[item]['data']['DevEUI_uplink']['payload_hex']
        fill_data['lat'] = data[item]['data']['DevEUI_uplink']['LrrLAT']
        fill_data['long'] = data[item]['data']['DevEUI_uplink']['LrrLON']
        fill_data['ts'] = ISODate(data[item]['data']['DevEUI_uplink']['Time'])
        fill_data['team'] = data[item]['team']

        tmp.push(fill_data)
    }
    return tmp
}
async function cleansing() {
    try {
        let collection = db.collection('pm25_raw_data');
        var query = { 'isClean' : false };
        collection.find(query).toArray(function (err, result) {
            if (err) throw err;

            var fillter_data =  make_data_fillter(result).then((resolve , reject) => {
                //console.log(resolve);
                if (Array.isArray(resolve) && resolve.length){
                    db.collection("pm25_fill_data").insertMany(resolve, function(error, res) {
                        if(error) {
                            console.error(error)    
                        } else {
                            console.log("Insert data success");
                        }
                    })
                }
                

            });
        })

        var newvalues = {$set: {'isClean' : true} };
        collection.updateMany(query, newvalues, function(err, res) {
            if (err) throw err;
        });
    } catch (err) {
        console.log(err);
    }

    try {
        let collection = db.collection('track_raw_data');
        var query = { 'isClean' : false };
        collection.find(query).toArray(function (err, result) {
            if (err) throw err;

            var fillter_data =  make_data_fillter(result).then((resolve , reject) => {
                //console.log(resolve);
                if (Array.isArray(resolve) && resolve.length){
                    db.collection("track_fill_data").insertMany(resolve, function(error, res) {
                        if(error) {
                            console.error(error)    
                        } else {
                            console.log("Insert data success");
                        }
                    })
                }
                

            });
        })

        var newvalues = {$set: {'isClean' : true} };
        collection.updateMany(query, newvalues, function(err, res) {
            if (err) throw err;
        });
    } catch (err) {
        console.log(err);
    }

}

mqtt_client.on('connect', function () {
    console.log("MQTT client connect to server at " + config.mqtt.host + " success.")

    mqtt_client.on('close', function () {
        console.error("MQTT connection closed.")
    })

    mqtt_client.subscribe('tgr2020/pm25/data/#')
    mqtt_client.subscribe('tgr2020/track/data/#')
})

mqtt_client.on('reconnect', function () {
    console.error("MQTT client re-connecting.")
})

mqtt_client.on('message', function (topic, message) {
        //console.log(topic.toString() + " => " + message.toString())
        let data_type = topic.split( "/" )[1]
        let team = topic.split( "/" )[3]
        tmp = {'data' : JSON.parse(message) , 'isClean' : false , 'team' : team }
        write_sensor_data(tmp , data_type);
})

mqtt_client.on('error', function (error) {
	console.error(error)
})

setInterval(cleansing, 1000);
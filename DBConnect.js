const DBCONNECT = require('mongodb').MongoClient;
const URL = 'mongodb://tgr01_db:tgr01db@202.139.192.114:27017/TGR01_DB_Day01'

const OPTIONS = {
  useNewUrlParser: true,
  useUnifiedTopology: true
};

DBCONNECT.connect(URL ,OPTIONS,(err , result) => {
  if(err)
    throw err
  else {
    console.log("MongoDB connection Success")
  }
  module.exports = result;
})

module.exports = DBCONNECT;

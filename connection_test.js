const DBCONNECT = require('./DBConnect')
var mqtt   = require('mqtt');
var client = mqtt.connect('mqtt://202.139.192.75/tgr2020/');

const Schema = DBCONNECT.Schema

const testSchema = new Schema({
    name:{
        type: String
    },
    val:{
        type: Number
    }
},{versionKey:false,timestamps:true})

const TestCollection = DBCONNECT.model('test_connection',testSchema)
const data = new TestCollection({
    name:'Data from testConnect',
    val: 0
})

client.on('connect', function (res) {
    console.log("MQTT Connected.");
})
client.on('error', function (error) {
    console.log("Error");
})
client.on('close', function (err) {
      console.log("Close")
})

data.save().then((result)=>{
    console.log(result)
    process.exit(0);
}).catch((error)=>{
    console.log(error)
    process.exit(1);
})

const express = require('express');
var path = require('path');
const fs = require('fs')
const ini = require('ini')
const mqtt = require('mqtt')
const mongodb = require('mongodb')
var ISODate = require("isodate");
var Pusher = require('pusher');
const  tf = require('@tensorflow/tfjs')
const tfnode = require('@tensorflow/tfjs-node');


var pusher = new Pusher({
appId: '929287',
key: 'd737e0baa86924a8398d',
secret: 'f32a32cc731a200596f7',
cluster: 'ap1',
encrypted: true
});

var config = ini.parse(fs.readFileSync(__dirname + '/config.ini', 'utf-8'))
var mqtt_client = mqtt.connect("mqtt://" + config.mqtt.host, {username: config.mqtt.username, password: config.mqtt.password, clientId: 'tgr01', connectTimeout: 3000, reconnectPeriod: 3000} , clientId = "TGR01")

const app = express();
const port = process.env.PORT || 80;

var mongo_client = mongodb.MongoClient
var db

mongo_client.connect(config.mongodb.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}, function(error, connection) {
    if (error) {
        console.error(error)
    } else {
        db = connection.db("TGR01_DB_Day01");
    }
})

app.use(express.static(path.join(__dirname,'./')));

function write_sensor_data(data , collection ) {
    if(collection == "pm25")
    {
        db.collection("pm25_raw_data").insertOne(data, function(error, res) {
            if(error) {
                console.error(error)    
            } else {
                //console.log("Insert data success");
            }
        })
    }

    if(collection == "track")
    {
        if(data['data']['event_code'] >= 1)
        {
            pusher.trigger('my-channel', 'my-event', {
                "message": "Danger!!!!!!"
                });
        }
        db.collection("track_raw_data").insertOne(data, function(error, res) {
            if(error) {
                console.error(error)    
            } else {
                //console.log("Insert data success");
            }
        })
    }
}

function write_bad_data(data , collection ) {
    if(collection == "pm25")
    {
        db.collection("pm25_bad_data").insertOne(data, function(error, res) {
            if(error) {
                console.error(error)    
            } else {
                //console.log("Insert data success");
            }
        })
    }

    if(collection == "track")
    {
        db.collection("track_bad_data").insertOne(data, function(error, res) {
            if(error) {
                console.error(error)    
            } else {
                //console.log("Insert data success");
            }
        })
    }
}

async function make_data_fillter(data , collection)
{

    //console.log(data)
    if(collection == "pm25")
    {
        var tmp = []
        for (var item in data) {

            fill_data = {}

            //fill_data['raw_data_id'] = data[item]['data']['_id']
            fill_data['device_id'] = data[item]['data']['DevEUI_uplink']['DevEUI']
            fill_data['device_addr'] = data[item]['data']['DevEUI_uplink']['DevAddr']
            fill_data['payload'] = parseInt( data[item]['data']['DevEUI_uplink']['payload_hex'].slice(2,4) , 16)
            fill_data['lat'] = data[item]['data']['DevEUI_uplink']['LrrLAT']
            fill_data['long'] = data[item]['data']['DevEUI_uplink']['LrrLON']
            fill_data['ts'] = ISODate(data[item]['data']['DevEUI_uplink']['Time'])
            fill_data['team'] = "tgr" + data[item]['team']

            tmp.push(fill_data)
        }
        return tmp
    }

    if(collection == "track")
    {
        var tmp = []
        for (var item in data) {

            fill_data = {}

            // //fill_data['raw_data_id'] = data[item]['data']['_id']
            fill_data['mac_addr'] = data[item]['data']['mac_addr']
            fill_data['rssi'] = data[item]['data']['rssi']
            fill_data['timestamp'] = data[item]['data']['timestamp']
            fill_data['event_code'] = data[item]['data']['event_code']
            // fill_data['long'] = data[item]['data']['DevEUI_uplink']['LrrLON']
            fill_data['ts'] = new Date().toISOString()
            fill_data['team'] = "tgr" + data[item]['team']

            tmp.push(fill_data)
        }
        return tmp
    }

    
}
async function cleansing() {
    try {
        let collection = db.collection('pm25_raw_data');
        var query = { 'isClean' : false };
        collection.find(query).toArray(function (err, result) {
            //if (err) throw err;
            make_data_fillter(result , "pm25").then((resolve , reject) => {
                //console.log(resolve);
                if (Array.isArray(resolve) && resolve.length){
                    db.collection("pm25_fill_data").insertMany(resolve, function(error, res) {
                        if(error) {
                            console.error(error)    
                        } else {
                            console.log("Insert data success");
                        

                        }
                        
                    })

                    var newvalues = {$set: {'isClean' : true} };
                    collection.updateMany(query, newvalues, function(err, res) {
                        //if (err) throw err;
                     });
                }
                

            });
        })

    } catch (err) {
        console.log(err);
    }


    try {
        let collection = db.collection('track_raw_data');
        var query = { 'isClean' : false };
        collection.find(query).toArray(function (err, result) {
            //if (err) throw err;

            var fillter_data =  make_data_fillter(result , "track").then((resolve , reject) => {
                //console.log(resolve);
                if (Array.isArray(resolve) && resolve.length){
                    db.collection("track_fill_data").insertMany(resolve, function(error, res) {
                        if(error) {
                            console.error(error)    
                        } else {
                            console.log("Insert data success");
                                                }
                    })

                    var newvalues = {$set: {'isClean' : true} };
                    collection.updateMany(query, newvalues, function(err, res) {
                        //if (err) throw err;
                     });
                }
                

            });
        })
    } catch (err) {
        console.log(err);
    }
}

mqtt_client.on('connect', function () {
    console.log("MQTT client connect to server at " + config.mqtt.host + " success.")

    mqtt_client.on('close', function () {
        console.error("MQTT connection closed.")
    })

    //pm2.5 data can subsrcibe from tgr2020/pm25/data/44 - 47
    mqtt_client.subscribe('tgr2020/pm25/data/44');
    mqtt_client.subscribe('tgr2020/pm25/data/45');
    mqtt_client.subscribe('tgr2020/pm25/data/46');
    mqtt_client.subscribe('tgr2020/pm25/data/47');

    mqtt_client.subscribe('tgr2020/track/data/1');
    //mqtt_client.subscribe('tgr2020/track/data/16');
    //mqtt_client.subscribe('tgr2020/track/data/34');
})

mqtt_client.on('reconnect', function () {
    console.error("MQTT client re-connecting.")
})

mqtt_client.on('message', function (topic, message) {
        //console.log(topic.toString() + " => " + message.toString())
        let data_type = topic.split( "/" )[1]
        let team = topic.split( "/" )[3]
       

        try {
            tmp = { 'data' : JSON.parse(message) , 'isClean' : false , 'team' : team }
            write_sensor_data(tmp , data_type);

        } catch (e) {
            //console.error(message);
            tmp = {'data' : message , 'ts' : new Date().toISOString()  , 'team' : team }
            write_bad_data( tmp ,data_type )

        }
})

mqtt_client.on('error', function (error) {
	console.error(error)
})

app.get('/api/track_data/:sensor_id' , (req , res) => {
    if(req.params.sensor_id == 'list')
    {
        //console.log(req.query)
        try {
            let collection = db.collection('track_fill_data');
            var query = { 'ts' : { "$gte": new Date(req.query.start)  , "$lte": new Date(req.query.end) }};
            collection.find(query).limit(parseInt(req.query.limit)).skip(parseInt(req.query.skip)).toArray(function (err, result) {
                if (err) throw err;
                res.send(result)
            })
        } catch (err) {
            console.log(err);
        }
    }
    else
    {
        try {
            let collection = db.collection('track_fill_data');
            var query = { 'device_addr' : req.params.sensor_id };
            collection.find(query).toArray(function (err, result) {
                if (err) throw err;
    
                res.send(result[result.length - 1])
            })
        } catch (err) {
            console.log(err);
        }
    }

    
});
app.get('/api/pm25/:sensor_id' , (req , res) => {
    if(req.params.sensor_id == 'list')
    {
        try {
            let collection = db.collection('pm25_fill_data');
            var query = { 'ts' : { "$gte": new Date(req.query.start)  , "$lt": new Date(req.query.end)} };
            collection.find(query).limit(parseInt(req.query.limit)).skip(parseInt(req.query.skip)).toArray(function (err, result) {
                if (err) throw err;

                res.send(result)
            })
        } catch (err) {
            console.log(err);
        }
    }
    else{
        try {
            let collection = db.collection('pm25_fill_data');
            var query = { 'device_addr' : req.params.sensor_id };
            collection.find(query).toArray(function (err, result) {
                if (err) throw err;
    
                res.send(result[result.length - 1])
            })
        } catch (err) {
            console.log(err);
        }
    }
    
});


app.get('/' , (req , res) => {
    res.redirect(`/map/`);
});
  
app.get('/map/' , (req , res) => {
    res.sendFile(__dirname + '/Hello.html');
});

app.get('/map/mapdata' , (req , res) => {
    var file = fs.readFileSync(__dirname + "/mapData.json", "utf-8");
    res.json(JSON.parse(file));
});

function markerUpdate()
{
    try
    {
        var obj = {}
        var teamCheck = []
        var query= db.collection("pm25_fill_data").find().sort({ts: -1}).toArray((err,result)=>{
            //console.log(Object.keys(result).length)
            if(Object.keys(result).length > 0)
            {
                for(var x in result){
                    //console.log(result[x]);
                    if(teamCheck.indexOf(result[x]["team"]) == -1)
                    {
                      obj[x] = result[x];
                      teamCheck.push(result[x]["team"])
                    }
                    if(teamCheck.length == 4)
                    {
                      break;
                    }
                  }

                  var json = JSON.stringify(obj);
                  fs.writeFile('mapData.json', json, 'utf8', (err,result)=>{
                     if(err) throw err;
                     else{
                      //console.log("Json File Created!");
                     }
                  });
            }
          //console.log(teamCheck);    
        });
    }
    catch(err)
    {

    }
}


//Prediction API
function denormalise(tensor,min,max){
    const denormalisetensor =tensor.mul(max.sub(min)).add(min);
    return denormalisetensor;
}
function normalise(tensor,previousMin=null,previousMax=null){
    const min = previousMin|| tensor.min();
    const max = previousMax||tensor.max();
    const normalisedTensor =tensor.sub(min).div(max.sub(min));
    return  {
        tensor:normalisedTensor
        ,min,max
  
    };
  
}
app.get('/model' , (req , res) => {
    var file = fs.readFileSync(__dirname + "/model.json", "utf-8");
    res.json(JSON.parse(file));
});

async function processModel(req , res){
    const model = await tf.loadLayersModel('http://localhost/model/model.json');
    coor_result = [
        {'lat' : 19.011294 , 'lng' : 99.887926},
        {'lat' : 19.156391 , 'lng' : 99.891908},
        {'lat' : 19.167149 , 'lng' : 99.87926},
        //{'lat' : 19.021293 , 'lng' : 99.897925},
    ]
        optimizer = tf.train.adam()
        model.compile({  loss: 'categoricalCrossentropy', optimizer : 'adam', metrics: ['accuracy']});

        var obj = [99,99,99]
        var teamCheck = []
        var query = db.collection("track_fill_data").find({}).sort({ts: -1}).toArray((err,result)=> {
            if(Object.keys(result).length > 0)
            {
                for(var x in result){
                    //console.log(result[x]);
                    if(teamCheck.indexOf(result[x]["mac_addr"]) == -1)
                    {
                        if(result[x]["mac_addr"] == "80:E1:26:07:E1:BF")
                        {
                            obj[0] = result[x]['rssi'];
                        }
                        if(result[x]["mac_addr"] == "80:E1:26:00:B8:89")
                        {
                            obj[1] = result[x]['rssi'];
                        }
                        if(result[x]["mac_addr"] == "80:E1:26:07:C7:BE")
                        {
                            obj[2] = result[x]['rssi'];
                        }
                        
                        teamCheck.push(result[x]["mac_addr"])
                    }
                  }
            }
            min_rssi = Math.min(...obj)
            //console.log(min_rssi)
            for(var i in obj)
            {
                i = parseInt(i)
                if(i == 0 && obj[i] == 99)
                {
                    input[i] = min_rssi
                }
                if(i == 1 && obj[i] == 99)
                {
                    input[i] = min_rssi
                }
                if(i == 2 && obj[i] == 99)
                {
                    input[i] = min_rssi
                }
            }
            let xv=tf.tensor2d([obj])

            let normalisedTestTensor=normalise(xv);
            xv = normalisedTestTensor.tensor

            var yv = model.predict(xv).arraySync()[0];

            console.log(yv)
            max_val = Math.max(...yv)
            //console.log(max_val);
            index_max = yv.indexOf(max_val)
            console.log(index_max)

            if(index_max == -1)
            {
                res.json({'val' : coor_result[0]})
            }
            else
            {
                coor = coor_result[index_max]
                res.json({'val' : coor});
            }

    })
}

app.get('/map/predict' , async (req , res) => {

    //19.036098, 99.883563
    
    processModel(req , res)
    
})


//Run Server on PORT 80
app.listen(port , () => {
    console.log(`App is running on port ${port}`);
    setInterval(async function () {await cleansing()}, 5000);
    setInterval( markerUpdate, 10000);
  });
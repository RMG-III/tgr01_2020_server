var mqtt   = require('mqtt');
var client = mqtt.connect('mqtt://167.99.68.69');
 
client.on('connect', function () {
    console.log("MQTT Connected.");

    setInterval(function() {
        
        let data = JSON.parse({"DevEUI_uplink":{"Time":"2020-01-08T13:09:42.39+07:00","DevEUI":"BE7A000000000409","DevAddr":"BE7A0009","FPort":"2","FCntUp":"90","ADRbit":"1","MType":"4","FCntDn":"90","payload_hex":"00000000000000fe000000","mic_hex":"1f96ff96","Lrcid":"00000231","LrrRSSI":"-111.000000","LrrSNR":"4.750000","SpFact":"7","SubBand":"G1","Channel":"LC16","DevLrrCnt":"1","Lrrid":"1000018F","Late":"0","LrrLAT":"19.027521","LrrLON":"99.891396","Lrrs":{"Lrr":[{"Lrrid":"1000018F","Chain":"0","LrrRSSI":"-111.000000","LrrSNR":"4.750000","LrrESP":"-112.254700"}]},"CustomerID":"1100001293","CustomerData":{"alr":{"pro":"LORA/Generic","ver":"1"}},"ModelCfg":"0","InstantPER":"0.000000","MeanPER":"0.000000"}})
        client.publish('tgr2020/pm25/data/01', data);
        console.log("publish new data")
    }, 200)
})
